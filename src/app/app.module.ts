import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/pages/home-page/home-page.component';
import { PhotograferPageComponent } from './components/pages/works/photografer-page/photografer-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'work-photografer', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  { path: 'work-photografer', component: PhotograferPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    PhotograferPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
